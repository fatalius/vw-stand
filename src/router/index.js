import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/HelloWorld'
import Babylon from '@/components/Babylon'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/babylon',
    name: 'babylon',
    component: Babylon
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
